# Perl6 experiments with GeoIP2, GeoIP::City and Geo::IP2Location::Lite

## Overview

Here's simple examples for location lookup by IP via Perl6 modules GeoIP2, GeoIP::City and Geo::IP2Location::Lite.

## License information

These scripts are free and opensource software, so you can redistribute them and/or modify them under the terms of the Artistic License 2.0.

## Links and credits

[Pheix brew site](https://perl6.pheix.org)

[GeoIP2](https://github.com/bbkr/GeoIP2)

[GeoIP::City](https://github.com/bbkr/GeoIPerl6)

[Geo::IP2Location::Lite in Perl 6](https://github.com/leejo/geo-ip2location-lite-p6)

[MaxMind GeoLite City database](https://dev.maxmind.com/geoip/legacy/geolite/)

[IP2Location Lite](https://lite.ip2location.com/)

[The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0)

## Contact

Please contact us via [feedback form](https://pheix.org/feedback.html) at pheix.org
