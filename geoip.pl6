#!/usr/bin/env perl6

use v6;
use GeoIP::City;

# my $gc = GeoIP::City.new;
my $gc = GeoIP::City.new( directory => "./ipdb/" );

# check databases
$gc.info.say;

# loop over etalon values file
for "./data-ip.txt".IO.lines -> $line {
    $line ~~ s:Perl5:g/[\r\n]+//;
    my @dat  = $line.split(":",:skip-empty);
    my $c = $gc.locate(@dat[0])<country_code>;
    if $c eq @dat[1] {
        say @dat[0] ~ " is located in " ~ $c ~ " \{" ~ @dat[1] ~ "\}";
    } else {
        say "***Err: " ~ @dat[0] ~ ": location is undefined \{" ~ @dat[1] ~ "\}";
    }
}
