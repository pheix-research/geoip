#!/usr/bin/env perl6

use v6;
use GeoIP2;

my $gc = GeoIP2.new( path => './ipdb/GeoLite2-City.mmdb' );;

# loop over etalon values file
for "./data-ip.txt".IO.lines -> $line {
    $line ~~ s:Perl5:g/[\r\n]+//;
    my @dat  = $line.split(":",:skip-empty);
    my $c = $gc.locate( ip => @dat[0] )<country><iso_code>;
    if $c eq @dat[1] {
        say @dat[0] ~ " is located in " ~ $c ~ " \{" ~ @dat[1] ~ "\}";
    } else {
        say "***Err: " ~ @dat[0] ~ ": location is undefined \{" ~ @dat[1] ~ "\}";
    }
}
