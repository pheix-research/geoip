#!/usr/bin/env perl6

# This is example of IP2Location Lite DB programming

use v6;
use Geo::IP2Location::Lite;

my $obj = Geo::IP2Location::Lite.new(file => "./iplocation-lite.29-Jan-2018.bin");

for "./data-ip.txt".IO.lines -> $line {
    $line ~~ s:Perl5:g/[\r\n]+//;
    my @dat  = $line.split(":",:skip-empty);
    my $c = $obj.get_country_short( @dat[0] );
    if $c eq @dat[1] {
        say @dat[0] ~ " is located in " ~ $c ~ " \{" ~ @dat[1] ~ "\}";
    } else {
        say "***Err: " ~ @dat[0] ~ ": location is undefined \{" ~ @dat[1] ~ "\}";
    }
}
