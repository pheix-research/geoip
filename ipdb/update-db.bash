#!/usr/bash

RED='\e[0;31m'
GREEN='\e[0;32m'
YELLOW='\e[0;33m'
NC='\e[0m'

if ls ./*.dat 1> /dev/null 2>&1; then
	rm -rf ./*.dat
	echo -e "[ ${GREEN}old ./*.dat files are removed${NC} ]"
else
	echo -e "Skip delete of ${YELLOW}./*.dat${NC}: not existed"
fi

if ls ./*.mmdb 1> /dev/null 2>&1; then
	rm -rf ./*.mmdb
	echo -e "[ ${GREEN}old ./*.mmdb files are removed${NC} ]"
else
	echo -e "Skip delete of ${YELLOW}./*.mmdb${NC}: not existed"
fi

curl -LJO http://geolite.maxmind.com/download/geoip/database/GeoLiteCityv6-beta/GeoLiteCityv6.dat.gz

if [ $? -eq 0 ]
then
	gzip -d GeoLiteCityv6.dat.gz
	rm -f GeoLiteCityv6.dat.gz
	ln -s GeoLiteCityv6.dat GeoIPCityv6.dat
	echo -e "[ ${GREEN}GeoLiteCityv6-beta is proceed${NC} ]"
else
	echo -e "[ ${RED}GeoLiteCityv6-beta database download error${STAGE}${NC} ]"
	exit -1;
fi

curl -LJO http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz

if [ $? -eq 0 ]
then
	gzip -d GeoLiteCity.dat.gz
	rm -f GeoLiteCity.dat.gz
	ln -s GeoLiteCity.dat GeoIPCity.dat
	echo -e "[ ${GREEN}GeoLiteCity database is proceed${NC} ]"
else
	echo -e "[ ${RED}GeoLiteCity database download error${STAGE}${NC} ]"
	exit -1;
fi

geoipupdate -d .

if [ $? -eq 0 ]
then
	echo -e "[ ${GREEN}GeoLite2-City and GeoLite2-Country databases is proceed${NC} ]"
else
	echo -e "[ ${RED}GeoLite2-City and GeoLite2-Country databases update via geoipupdate error${STAGE}${NC} ]"
	exit -1;
fi

