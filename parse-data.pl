#!/usr/bin/env perl

use strict;
use warnings;

my $fname = "bigbro.tnk";

open my $fh, "<", $fname or die 'unable to open file ' . $fname . ': ' . $!;
my @_f = <$fh>;
close $fh;

my @d;

foreach (@_f) {
    my $_r = $_;
    $_r =~ s/[\r\n\t]+//g;
    my @_t = split /\|/, $_r;
    my @_dd = map { $_ } grep { $_ =~ /$_t[2]/ } @d;
    if ( !@_dd ) {
        push @d, $_t[2] . ":" . $_t[-1];
    }
}

print join "\n", @d;